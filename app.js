//this is synchronous func
/*function otherFunc(){
	console.log(`Inside other function`);
	console.log(`Some stuff`)
}

console.log('Start');

otherFunc();

console.log(`End`)*/

// Asynchronous func



/*const stocks = {
	fruits: [`Strawberry`, `Grapes`, `Banana`, `Apple`],
	liquid: [`Water`, `Ice`],
	holder: [`Cone`, `Cup`, `Stick`],
	toppings: [`Chocolate`, `Peanuts`]
}

const order = (fruit, prodFunc) => {
	setTimeout(() => {
		console.log(`Customer order: ${stocks.fruits[fruit]}`)
		prodFunc(fruit);
	}, 2000);
};

const production = (fruit) => {
	setTimeout(() => {
		console.log(`Production for ${stocks.fruits[fruit]} has started`)

		setTimeout(() => {
			console.log(`${stocks.fruits[fruit]} has been chopped now`)
	
			setTimeout(() => {
				console.log(`${stocks.liquid[0]} and ${stocks.liquid[1]} are now added`)
	
				setTimeout(() => {
					console.log(`The machine started for production`)
	
					setTimeout(() => {
						console.log(`${stocks.holder[1]} will be used for ${stocks.fruits[fruit]}`)

						setTimeout(() => {
							console.log(`${stocks.toppings[0]} will be the toppings for the customer's ${stocks.fruits[fruit]} ice cream`)

							setTimeout(() => {
								console.log(`Customer's ${stocks.fruits[fruit]} ice cream is now ready!`)
							}, 3000)
						}, 3000)
					}, 2000)
				},1000)
			}, 1000)
		}, 2000)
	}, 0000)
};

order(0, production)*/

//Promises

/*const stocks = {
	fruits: [`Strawberry`, `Grapes`, `Banana`, `Apple`],
	liquid: [`Water`, `Ice`],
	holder: [`Cone`, `Cup`, `Stick`],
	toppings: [`Chocolate`, `Peanuts`]
}

const is_shop_open = true;

const order = (time, work) => {
	return new Promise( (resolve, reject) => {
		if(is_shop_open){
			setTimeout(() => {
				resolve(work())

			}, time)
		}
		else{
			reject(console.log(`We apologize, the store is closed.`))
		}
	} )
};

order(2000, () => {console.log(`Customer selected ${stocks.fruits[0]} flavor`)})
.then(() => {
	return order(0000, () => {console.log(`Production for ${stocks.fruits[0]} ice cream has started`)})
})
.then(() => {
	return order(2000, () => {console.log(`${stocks.fruits[0]} has been chopped`)})
})
.then(() => {
	return order(1000, () => {console.log(`${stocks.liquid[0]} and ${stocks.liquid[1]} are added`)})
})
.then(() => {
	return order(1000, () => {console.log(`Machine has started`)})
})
.then(() => {
	return order(2000, () => {console.log(`Customer chose ${stocks.holder[1]}`)})
})
.then(() => {
	return order(3000, () => {console.log(`${stocks.toppings[0]} will be the topping`)})
})
.then(() => {
	return order(2000, () => {console.log(`${stocks.fruits[0]} ice cream has been served`)})
})
.catch(() => {
	console.log(`Customer left`)
})
.finally(() => {
	console.log(`The store is now closing`)
})*/

//Async and await

const stocks = {
	fruits: [`Strawberry`, `Grapes`, `Banana`, `Apple`],
	liquid: [`Water`, `Ice`],
	holder: [`Cone`, `Cup`, `Stick`],
	toppings: [`Chocolate`, `Peanuts`]
}

const is_shop_open = true;

/*const order = () => {
	return new Promise((resolve, reject) => {
		if(is_shop_open){
			resolve()
		}
		else{
			reject()
		}
	})
}*/

/*async function order(){
	try{
		await abc;
	}
	catch(error){
		// return error.message;
		console.log(`abc does not exist`, error)
	}
	finally{
		console.log(`Finally block runs anyways`)
	}
}

// const order = async() => {

// }

order()
.then(() => {console.log(`Trying`)})*/

/*const toppingsChoice = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {resolve(console.log(`which topping do you like`))}, 3000)
	})
}

const kitchen = async() => {
	console.log(`A`);
	console.log(`B`);
	console.log(`C`);

	await toppingsChoice()

	console.log(`D`);
	console.log(`E`);
}

kitchen();

console.log(`Cleaning dishes`);
console.log(`Cleaning tables`);
console.log(`Taking orders`);*/

function time(ms){
	return new Promise((resolve, reject) => {
		if(is_shop_open){
			setTimeout(() => {
				resolve(console.log(`Working`))
			}, ms)
		}
		else{
			reject(console.log(`Shop is closed`));
		}
	})
}

async function kitchen(){
	try{
		await time(2000)
		console.log(`${stocks.fruits[0]}`)
	}
	catch(error){
		console.log(`Customer left`, error)
	}
	finally{
		console.log(`Store is now closing`)
	}
}

kitchen()